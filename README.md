# Evaluate two control software implementation paradigms
Spend 30-90 minutes on this. Really, aim for 1 hour or less - there isn’t a “right” answer. The point is to consider the purpose of structure in implementation such that you and an interviewer can have a discussion about choices and what you would do differently if you had 10 hours or for other scenarios.

First, decide whether to consider examples in Python or C++.
* For Python, both examples are in the file `controller-paradigm.py`. 
* For C++, one methodology is in `controller-paradigm.cpp` and the other is in `controller-paradigm2.cpp`.

Next, consider a general form for a controller.
```math
x[k+1]=f(x[k], y[k])\\
u[k]=g(x[k], y[k])
```
Where y represents sensor measurements from a plant system, x is the internal state of the controller, u is the set of control commands intended for actuators, and f and g are functions.

There are many possible implementations in software. Two will be presented here. Your tasks are to

1. Summarize your experience testing electromechanical systems and implementing controllers in software (i.e. C++, Python, Simulink, etc.). This provides valuable context for evaluating the following questions.
2. Compare and contrast the positive and negative aspects of the provided examples. Both have desirable characteristics, trade-offs, and even a few flaws. Readability, maintainability, scalability, testability, and performance are a few possible metrics for evaluation but you are free to provide your own criteria.
3. Describe how you would test a controller written in each paradigm. Consider both unit, integration, and behavior/verification tests.
4. Choose one of the two and adapt it to make it better. This could be anything from adding comments to starting over. Dealers choice.

Testing was performed in Ubuntu 20.04 with gcc 9.3.0 and Python 3.8.10.
