class Controller():

    def __init__(self, z0):
        self._z = z0
    
    def run(self, y):
        self._z = self._update_states(y)
        return self._calculate_inputs(y)
    
    def _update_states(self):
        pass

    def _calculate_inputs(self):
        return None


class Plant():

    def __init__(self, f, g):
        self.__f = f
        self.__g = g

    def derivative(self, x, u):
        return self.__f(x, u)

    def outputs(self, x, u):
        return self.__g(x, u)


class Solver():

    def __init__(
            self,
            c: Controller,
            p: Plant,
            s,
            ):
        self._c = c
        self._p = p
        self._s = s

    def sim(self, x, u, T=10, Ts=1e-3, t0=0):
        t = t0
        while t <= T:
            y = self._p.outputs(x, u)
            u = self._c.run(y)
            x = self._s(lambda z: self._p.derivative(z, u), x, Ts)
            t += Ts
        return self._p.outputs(x, u)


class PID(Controller):
    p_gain = 0.1
    i_gain = 0
    d_gain = 0

    def __init__(self, z0, Ts=1e-3):
        super(PID, self).__init__(z0)
        self.__Ts = Ts

    def _update_states(self, e):
        self._z[0] = self._z[0] + e
        self._z[2] = self._z[1]
        self._z[1] = e

    def _calculate_inputs(self, e):
        dedt = (self._z[1] - self._z[2])/self.__Ts
        return -self.p_gain*e - self.i_gain*self._z[0] - self.d_gain*dedt

    def run(self, y):
        self._update_states(y)
        return self._calculate_inputs(y)


def forward_euler(dxdt, x, T):
    new_x = x
    d = dxdt(x)
    for i in range(len(x)):
        new_x[i] = d[i]*T + x[i]
    return new_x

# x[0]: position
# x[1]: velocity
def my_derivative(x, u):
    return [
        x[1], # xdot
        u, # xddot = F/m
    ]

def my_outputs(x, u):
    return x[0]

def main():
    Ts = 1e-3
    c = PID([0, 0, 0], Ts)
    p = Plant(my_derivative, my_outputs)
    s = Solver(c, p, forward_euler)
    x0 = [1, 1]
    u0 = 0

    print(s.sim(x0, u0, Ts=Ts))


if __name__ == "__main__":
    # execute only if run as a script
    main()
