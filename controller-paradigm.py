class InternalStateController():

    def __init__(self, x0):
        self._x = x0
    
    def run(self, y):
        self._update_states(y)
        return self._calculate_inputs(y)
    
    def _update_states(self):
        pass

    def _calculate_inputs(self):
        return None


class InternalStatePID(InternalStateController):
    p_gain = 0.1
    i_gain = 0
    d_gain = 0

    def __init__(self, x0, Ts=1e-3):
        super(InternalStatePID, self).__init__(x0)
        self.__Ts = Ts

    def _update_states(self, e):
        self._x[0] = self._x[0] + self.i_gain*e
        self._x[2] = self._x[1]
        self._x[1] = e

    def _calculate_inputs(self, e):
        dedt = (self._x[1] - self._x[2])/self.__Ts
        return -self.p_gain*e - self._x[0] - self.d_gain*dedt


class ExternalStateController():

    def update(self, x, y):
        return f(x, y)

    def calculate_inputs(self, x, y):
        return g(x, y)


class ExternalStatePID(ExternalStateController):

    def __init__(
            self,
            Ts: float = 1e-3,
            p_gain: float = 0.1,
            i_gain: float = 0,
            d_gain: float = 0,
            ):
        super(ExternalStatePID, self).__init__()
        self.__Ts = Ts
        self.__p_gain = p_gain
        self.__i_gain = i_gain
        self.__d_gain = d_gain

    def update(self, x, e):
        x[0] = x[0] + self.__i_gain*e
        x[2] = x[1]
        x[1] = e
        return x

    def calculate_inputs(self, x, e):
        dedt = (x[1] - x[2])/self.__Ts
        return -self.__p_gain*e - x[0] - self.__d_gain*dedt


def main():
    Ts = 1e-3
    x = [0.123, 0.456, 0.789]
    y = 0.12345

    # Setup the internal state controller
    c_is = InternalStatePID(x.copy(), Ts)
    c_is.p_gain = 3.14159
    c_is.i_gain = 2.71828
    c_is.d_gain = 0.83463

    # Setup the external state controller
    c_es = ExternalStatePID(Ts, 3.14159, 2.71828, 0.83463)

    # Step the internal state controller
    # This would be in a loop in a full implementation
    u_is = c_is.run(y)

    # Step the external state controller
    # This would be in a loop in a full implementation
    x = c_es.update(x.copy(), y)
    u_es = c_es.calculate_inputs(x, y)

    # Check that they do the same thing numerically
    print(u_is, u_es)

if __name__ == "__main__":
    # execute only if run as a script
    main()
