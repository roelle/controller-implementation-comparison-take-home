#include <iostream>
#include <cmath>
#include <array>

struct Controller
{
    static constexpr int numStates {1};
    static constexpr int numOutputs {1};
    static constexpr int numInputs {1};
    std::array<double, numStates> x {0};

    std::array<double, numInputs>
    starting(std::array<double, numOutputs> y)
    {
        x[0] = x[0] + 3.14159; // Not a real algorithm
        std::cout << "Starting " << x[0] << std::endl;
        std::array<double, numInputs> u {0};
        u[0] = x[0];
        return u;
    }

    std::array<double, numInputs>
    running(std::array<double, numOutputs> y)
    {
        x[0] = x[0] +  2.71828; // Not a real algorithm
        std::cout << "Running " << x[0] << std::endl;
        std::array<double, numInputs> u {0};
        u[0] = x[0];
        return u;
    }

    std::array<double, numInputs>
    stopping(std::array<double, numOutputs> y)
    {
        x[0] = x[0] +  0.83463; // Not a real algorithm
        std::cout << "Stopping " << x[0] << std::endl;
        std::array<double, numInputs> u {0};
        u[0] = x[0];
        return u;
    }
};

void getMeasurements(std::array<double, Controller::numOutputs>& y)
{
    // Just making up a noise and measurement to get a number... not real
    double noise = std::cos(std::fmod((y[0] + 0.001)*3.14159265359*10, 1.0)*10.0);
    y[0] += 1 + noise;
    std::cout << "Get measurements " << y[0] << std::endl;
}

void setInputs(const std::array<double, Controller::numInputs> u)
{
    // commandActuators(u); // Didn't write this function... yet?
    std::cout << "Set actuators " << u[0] << std::endl;
}

int main()
{  
    Controller c;
    std::array<double, Controller::numOutputs> y {0};
    std::array<double, Controller::numInputs> u {0};
    enum Mode { start, run, stop, halt };
    Mode mode = start;
    
    while ( true )
    {
        getMeasurements(y);
        if ( mode == start )
        {
            u = c.starting(y);
            mode = run;
        }
        else if ( mode == run )
        {
            u = c.running(y);
            mode = stop;
        }
        else if ( mode == stop )
        {
            u = c.stopping(y);
            mode = halt;
        }
        else if ( mode == halt )
        {
            break;
        }        
        setInputs(u);
    }
}