#include <iostream>
#include <cmath>
#include <array>

template <typename T>
struct Controller
{
    static constexpr int numStates {1};
    static constexpr int numOutputs {1};
    static constexpr int numInputs {1};

    std::array<double, numInputs> run(
        const std::array<double, numOutputs> y,
        std::array<double, numStates>& x
    )
    {
        return static_cast<T*>(this)->update(y, x);
    }
    std::array<double, numInputs> update(
        const std::array<double, numOutputs> y,
        std::array<double, numStates>& x
    )
    {
        std::cout << "Executing Base Controller" << std::endl;
        std::array<double, numInputs> u {0};
        u[0] = x[0];
        return u;
    }
};

struct Starting: Controller<Starting>
{

    std::array<double, numInputs> update(
        const std::array<double, numOutputs> y,
        std::array<double, numStates>& x
    )
    {
        x[0] = x[0] + 3.14159; // Not a real algorithm
        std::cout << "Starting " << x[0] << std::endl;
        std::array<double, numInputs> u {0};
        u[0] = x[0];
        return u;
    }
};

struct Running: Controller<Running>
{

    std::array<double, numInputs> update(
        const std::array<double, numOutputs> y,
        std::array<double, numStates>& x
    )
    {
        x[0] = x[0] + 2.71828; // Not a real algorithm
        std::cout << "Running " << x[0] << std::endl;
        std::array<double, numInputs> u {0};
        u[0] = x[0];
        return u;
    }
};

struct Stopping: Controller<Stopping>
{

    std::array<double, numInputs> update(
        const std::array<double, numOutputs> y,
        std::array<double, numStates>& x
    )
    {
        x[0] = x[0] + 0.83463; // Not a real algorithm
        std::cout << "Stopping " << x[0] << std::endl;
        std::array<double, numInputs> u {0};
        u[0] = x[0];
        return u;
    }
};

struct Base: Controller<Base>{};

template <typename T>
auto execute(T& base, const std::array<double, 1> y, std::array<double, 1>& x)
{
    return base.run(y, x);
}

void getMeasurements(std::array<double, Base::numOutputs>& y)
{
    // Just making up a noise and measurement to get a number... not real
    double noise = std::cos(std::fmod((y[0] + 0.001)*3.14159265359*10, 1.0)*10.0);
    y[0] += 1 + noise;
    std::cout << "Get measurements " << y[0] << std::endl;
}

void setInputs(const std::array<double, Base::numInputs> u)
{
    // commandActuators(u); // Didn't write this function... yet?
    std::cout << "Set actuators " << u[0] << std::endl;
}

int main()
{  
    Base base;
    Starting starting;
    Running running;
    Stopping stopping;
    std::array<double, base.numStates> x {0};
    std::array<double, base.numOutputs> y {0};
    std::array<double, base.numInputs> u {0};
    enum Mode { start, run, stop, halt };
    Mode mode = start;
    
    while ( true )
    {
        getMeasurements(y);
        if ( mode == start )
        {
            u = execute(starting, y, x);
            mode = run;
        }
        else if ( mode == run )
        {
            u = execute(running, y, x);
            mode = stop;
        }
        else if ( mode == stop )
        {
            u = execute(stopping, y, x);
            mode = halt;
        }
        else if ( mode == halt )
        {
            break;
        }
        setInputs(u);
    }    
}